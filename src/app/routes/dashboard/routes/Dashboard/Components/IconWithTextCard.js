import React from "react";

import Widget from "components/Widget/index";

const IconWithTextCard = ({data}) => {
  const {cardColor, imageIcon, title, subTitle} = data;
  return (
    <Widget styleName={`p-3 bg-${cardColor} text-white cards_usuario`}>
      <div className="media align-items-center flex-nowrap py-lg-2">
        <div className="mr-3">
          <img className="img_marcas" src={imageIcon} alt={imageIcon}/>
        </div>
        <div className="media-body">
          <h4 className="jr-font-weight-black mb-1 text-black">{title}</h4>
          <p className="mb-0 jr-fs-cp cards_usuario">{subTitle}</p>
        </div>
      </div>
    </Widget>
  );
};

export default IconWithTextCard;
