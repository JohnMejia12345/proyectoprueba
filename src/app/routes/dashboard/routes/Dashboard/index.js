import React from 'react';
import {auth, firestore, userlogin} from '../../../../../firebase/firebase';
import ContainerHeader from 'components/ContainerHeader/index';
import CardBox from 'components/CardBox/index';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import Inputs from '../../../components/routes/textFields/inputs/Inputs';
import IntlMessages from 'util/IntlMessages';
import MaterialTable from 'material-table'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

import IconWithTextCard from './Components/IconWithTextCard'
import {Link} from "react-router-dom";
//import IntlMessages from '../../../../../util/IntlMessages';
//import ChartCard from '../../../../../components/dashboard/Common/ChartCard';
//import {ResponsiveContainer, LineChart} from '../../../../../recharts';
import {validateForms, newAttributes, setDateFormat} from 'actions/Helpers';
//import {Link} from "react-router-dom";
const crypto = require('crypto');


//se valida si la app está corriendo en modo pruebas o producción (se debe cambiar el dominio)
//se usa en caso de que se necesiten variables de configuración
const configuracion = window.location.hostname === "app.cemtrik.com" ? require('../../../../../config/prod') : require('../../../../../config/dev');

//const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

class Dashboard extends React.Component {
  //estados del componente
  constructor(props) {
    super(props);
    this.state = {
      alert: '',
      errors: [],
      alertData: ''
    };
  }

  componentDidMount() {


  }

  showAlert(type, message) {
    this.setState({
      alert: true,
      alertData: { type, message }
    });
    setTimeout(() => {
      this.setState({ alert: false });
    }, 4000)
  }

  render() {

    const {errors} = this.state;
    return (
      <div className="animated slideInUpTiny animation-duration-3">
        <ContainerHeader match={this.props.match} title={
          <IntlMessages id={`Hola!`}/>}/>
          {this.state.alert && <div className={`alert alert-${this.state.alertData.type}`} role='alert'>
            <div className='container'>
              {this.state.alertData.message}
            </div>
          </div>}

          <div className="col-xl-12 col-lg-8 col-md-12 col-12 order-sm-1">
            <div className="row">
              Dashboard
            </div>
          </div>
      </div>
    );
  }
}

export default Dashboard;
