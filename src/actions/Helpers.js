import {
  auth,
  firestore
} from '../firebase/firebase';

export function validateForms(states) {
  //se traen las reglas y el estado errors para los mensajes de errores
  let rules = states.rules;
  let errors = states.errors;
  let validForm = true;
  //se recorren las reglas del formulario para validar cada tipo de campo
  rules.map((rule) => {
    //si el campo es requerido se debe validar que no esté vacío el campo
    if(rule.required) {
      //se compara dinámicamente con el valor actual del estado (del campo)
      if(states[rule.field] === ""){
        //console.log(rule);
        errors[rule.field] = [rule.label]+" es requerido";
        validForm = false;
      } else {
        errors[rule.field] = "";
        //validForm = true;
      }
    }
  });

  return [
    errors,
    validForm
  ];
}

export function setDateFormat(fecha, formato) {
    //fecha puede ir null para formatear la fecha actual
     let fecha_sin_formatear = new Date(fecha);
     //si se le pasa formato procesa la fecha
     //sino se le pasa formato retorna el objeto de la fecha que se pasa en el método
     if(formato === "YYYY-MM-DD") {
       return fecha_sin_formatear.getFullYear()+ "-" + ('0' + (fecha_sin_formatear.getMonth()+1)).slice(-2) + "-" + ('0' + fecha_sin_formatear.getDate()).slice(-2);
     } else {
       return fecha_sin_formatear;
     }
}

export function newAttributes(){
  return {correo:auth.currentUser.email};
  /*return firestore.collection("usuarios").doc(auth.currentUser.uid).get()
  .then((doc) => {
    var data = {nombre:doc.data().nombres, correo:auth.currentUser.email};
    return data;
  })
  .catch((err)=>{console.log(err)});*/
}

export function testStates(states) {
  console.log(states);
}
